package com.demo.service;

import com.demo.mapper.User;

/**
 * @author xubin
 * @date 2017年9月1日 下午3:24:33
 */
public interface UserService {
	
	User getUserById(Long userId);
	
}
