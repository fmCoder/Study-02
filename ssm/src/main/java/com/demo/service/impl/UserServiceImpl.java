package com.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.dao.UserDao;
import com.demo.mapper.User;
import com.demo.service.UserService;

/**
 * @author xubin
 * @date 2017年9月1日 下午3:26:15
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	@Override
	public User getUserById(Long userId) {
		return this.userDao.selectByPrimaryKey(userId);
	}

}
