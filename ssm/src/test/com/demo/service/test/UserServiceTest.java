package com.demo.service.test;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.demo.service.UserService;

/**
 * @author xubin
 * @date 2017年9月1日 下午3:29:57
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/spring-mybatis.xml"})  
public class UserServiceTest {

	private static Logger logger = Logger.getLogger(UserServiceTest.class);  
	
	@Autowired
	private UserService userService;
	
	@Test
	public void testGetUser() {
		String username = userService.getUserById(6L).getUserName();
		logger.info("输出的UserName=" + username);
		assertEquals("admin", username);
	}
	
}
