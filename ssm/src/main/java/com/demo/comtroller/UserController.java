package com.demo.comtroller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.demo.mapper.User;
import com.demo.service.UserService;

/**
 * @author xubin
 * @date 2017年9月1日 下午4:01:50
 */
@Controller
@RequestMapping("user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping("/showUser")  
    public String toIndex(HttpServletRequest request, Model model){  
        Long userId = Long.parseLong(request.getParameter("id"));  
        User user = this.userService.getUserById(userId);
        model.addAttribute("user", user);  
        return "ShowUser";  
	}
	
}
